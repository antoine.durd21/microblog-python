** Instructions **
Fais un git clone du projet vers ta machine et suis les instructions suivantes

------------
Jimmy: Hey ! Salut à toi l'ingénieur SysOps, c'est Jimmy de l'équipe de developement

Jimmy : J'ai fais une super projet en Python qui s'appelle Microblog, cela crée un site web qui te permet de bloguer, dingue non ??

Jimmy : J'aimerai que tout le monde en profite, mais personne arrive à l'installer sur sa machine soit disant qu'ils ont pas les bonnes versions de python et blablalbla... Bref, tu veux pas m'aider à packager mon app dans un container Docker stp ? Je te donne les instructions

Jimmy : Tout d'abord, choisis une image Docker de base qui inclut Python 3.9 et qui soit légère. Il faut que l'image soit la plus rapide à télécharger possible.

Jimmy : Copie le fichier requirements.txt dans le conteneur, puis exécute la commande pip3 install -r requirements.txt pour installer toutes les dépendances nécessaires de mon application Python."

Jimmy : Définis la variable d'environnement FLASK_APP, qui a pour valeur microblog.py

Jimmy : Copie tout le contenu de mon programme que tu as téléchargé avec ton git clone dans le répertoire racine /microblog du conteneur, et définis ce répertoire comme le répertoire de travail

Jimmy : Ah oui, j'oubliais, il faut aussi une variable d'environnement CONTEXT avec la valeur PROD pour indiquer que tu es en mode production

Jimmy :  Mon programme écoute sur le port 5000

Jimmy : Pour finir, définis la commande par défaut qui sera exécutée lorsque le conteneur démarre. Dans notre cas, c'est le script boot.sh

Jimmy : Merci à toi, tu n'as plus qu'à "builder ce dockerfile" dans une image puis l'exécuter pour voir si cela fonctionne !
Jimmy : Tchouss !

This is an example application featured in Miguel Grinberg's [Flask Mega-Tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world). See the tutorial for instructions on how to work with it.

